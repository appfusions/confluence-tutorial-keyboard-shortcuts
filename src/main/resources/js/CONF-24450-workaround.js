// AJS.log(' +++ Keyboard shortcut workaround - https://jira.atlassian.com/browse/CONF-24450 +++ ');

AJS.toInit(function($) {
    AJS.I18n.get("com.appfusions.confluence.plugins.confluence-tutorial-keyboard-shortcuts");
});